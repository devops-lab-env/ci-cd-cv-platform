# CV Platform Project
This activity aims to understand CI/CD concepts through building and deploying a simple platform in Hybrid cloud.

GitLab stores source code of this project and its webhook notifies Jenkins, which installed on VM of Vagrant, the platform would be built on, then Jenkins deploys the platform on AWS-EC2 that helps simplify CI/CD Pipeline.

This project is deployed as a demo for my Homepage:
- [www.fadyebied.it](https://fadyebied.it)


# Architecture Vision
![](images/architecture-vision.png)

# Tools used to implement this architecture
## Setup:
- Virtualization (Vagrant, VirtualBox)
- Ubuntu Server 20.O4
- HTTP Apache (Proxy mode)
- Certbot (HTTPS)
## Development Tools:
### Backend Development tools
- Quarkus (Native mode)
- Maven
- Redis as (Database & Caching)
### Frontend Development tools
- VueJs 3
- Bootstrap
- jQuery
- npm
## Infrastructure as Code Tools:
### Infrastructure Provisioning
- Terraform
### Configuration Management
- Ansible
### CI/CD Tools
- Jenkins
- GitLab
## Cloud Provider
### AWS - EC2

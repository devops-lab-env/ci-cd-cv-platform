const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})

const webpack = require("webpack");
module.exports = {
  //configureWebpack is the place in Vue cli 3.0 that is used to configure the parameters of the webpack plug-in. You can set it here to create or overwrite the default configuration of webpack.
  //The meaning of webpack ProvidePlugin is to create a global variable that can be used in each module of webpack. The configuration meaning here is to create three variables' $','jquery' and 'window.jQuery' to point to jQuery dependency, and create a 'Popper' variable to point to popper.js dependency.
      configureWebpack: {
          plugins: [
              new webpack.ProvidePlugin({
                  $: 'jquery',
                  jQuery: 'jquery'
              })
          ]
        }
  }
  

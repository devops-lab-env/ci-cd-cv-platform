import { ref } from 'vue'


const getCv = () => {
    const jsonCv = ref([])
    const jsonCvLoad = ref([])
    const error = ref(null)

    const loadCv = async () => {
        try {
            let hostname = location.hostname;
            console.log(location.hostname);
            let data;
            if (hostname == "localhost"){
                data = await fetch('http://localhost:8080/api/cv')
            }else{
                data = await fetch('https://'+hostname+'/api/cv')
            }
            if (!data.ok) {
                throw Error('no data available')
            }
            jsonCvLoad.value = await data.json()
            jsonCv.value = jsonCvLoad.value.cv;
        } catch (err) {
            error.value = err.message
            console.log(err.message)
        }
    }
    return {jsonCv, error, loadCv}
}

export default getCv

import 'jquery-inview'
$(document).ready(function () {

    /*==========  Tooltip  ==========*/
    //$('.tool-tip').tooltip();
    
    /*==========  Progress Bars  ==========*/
    $('.progress-bar').on('inview', function (event, isInView) {
        if (isInView) {
            $(this).css('width',  function() {
                return ($(this).attr('aria-valuenow')+'%');
            });
        }
    });
    $('.dial').on('inview', function (event, isInView) {
        if (isInView) {
            var $this = $(this);
            var myVal = $this.attr("value");
            var color = $this.attr("data-color");
            $this.knob({
                readOnly: true,
                width: 200,
                rotation: 'anticlockwise',
                thickness: .05,
                inputColor: '#232323',
                fgColor: color,
                bgColor: '#e8e8e8',
                'draw' : function () { 
                    $(this.i).val(this.cv + '%')
                }
            });
            $({
                value: 0
            }).animate({
                value: myVal
            }, {
                duration: 1000,
                easing: 'swing',
                step: function() {
                    $this.val(Math.ceil(this.value)).trigger('change');
                }
            });
        }
    });


    /*==========  Responsive Navigation  ==========*/
    $('.main-nav').children().clone().appendTo('.responsive-nav');
    $('.responsive-menu-open').on('click', function(event) {
        event.preventDefault();
        $('body').addClass('no-scroll');
        $('.responsive-menu').addClass('open');
    });
    $('.responsive-menu-close').on('click', function(event) {
        event.preventDefault();
        $('body').removeClass('no-scroll');
        $('.responsive-menu').removeClass('open');
    });

    /*==========  Popup  ==========*/
    $('.share').on('click', function(event) {
        event.preventDefault();
        $('.popup').fadeToggle(250);
    });
    $('.slide-out-share').on('click', function(event) {
        event.preventDefault();
        $('.slide-out-popup').fadeToggle(250);
    });

    /*==========  Slide Out  ==========*/
    $('.header-action-button').on('click', function(event) {
        event.preventDefault();
        $('.slide-out-overlay').fadeIn(250);
        $('.slide-out').addClass('open');
    });
    $('.slide-out-close').on('click', function(event) {
        event.preventDefault();
        $('.slide-out-overlay').fadeOut(250);
        $('.slide-out').removeClass('open');
    });
    $('.slide-out-overlay').on('click', function(event) {
        event.preventDefault();
        $('.slide-out-overlay').fadeOut(250);
        $('.slide-out').removeClass('open');
    });

    /*==========  Search  ==========*/
    function positionSearch() {
        if ($(window).width() > $(window).height()) {
            var windowWidth = $(window).width();
            $('.search-overlay').css({'width': windowWidth*2.5, 'height': windowWidth*2.5});
        } else {
            var windowHeight = $(window).height();
            $('.search-overlay').css({'width': windowHeight*2.5, 'height': windowHeight*2.5});
        }
        var position = $('.header-open-search').offset();
        var height = $('.header-open-search').height();
        var width = $('.header-open-search').width();
        var top = position.top + height/2 - $('.search-overlay').outerHeight()/2;
        var left = position.left - width/2 - $('.search-overlay').outerWidth()/2;
        $('.search-overlay').css({'top': top, 'left': left});
    }
    positionSearch();
    $(window).on('resize', function() {
        positionSearch();
    });
    $('.open-search').on('click', function(event) {
        event.preventDefault();
        $('.search-overlay').addClass('scale');
        $('.search').addClass('open');
    });
    $('.search-close').on('click', function(event) {
        event.preventDefault();
        $('.search-overlay').removeClass('scale');
        $('.search').removeClass('open');
    });


});

import SilityStandardHome from '../../components/sility/standard/SilityAppStandardHome.vue'


export default [
    {
        path: '/',
        name: 'SilityStandardHome',
        component: SilityStandardHome
    },
    {
        path: '/about',
        name: 'SilityStandardAbout',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../../components/sility/standard/containers/About.vue')
    },
    {
        path: '/skill',
        name: 'SilityStandardSkill',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../../components/sility/standard/containers/Skill.vue')
    },
    {
        path: '/experience',
        name: 'SilityStandardExperience',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../../components/sility/standard/containers/Experience.vue')
    },
    {
        path: '/education',
        name: 'SilityStandardEducation',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../../components/sility/standard/containers/Education.vue')
    },
    {
        path: '/contact',
        name: 'SilityStandardContact',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../../components/sility/standard/containers/Contact.vue')
    }
]
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.27.0"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region     = "eu-central-1"
}

resource "aws_instance" "cv-platform" {
  ami             = "ami-065deacbcaac64cf2"
  instance_type   = "t2.micro"
  security_groups = ["launch-wizard-1"]
  key_name        = "cv-platform"

  tags = {
    Name = "cv-platform"
  }
}
output "instance_cv_platform_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.cv-platform.public_ip
}




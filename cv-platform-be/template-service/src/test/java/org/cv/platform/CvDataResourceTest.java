package org.cv.platform;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.cv.platform.models.CvData;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class CvDataResourceTest {

    @Test
    public void testRedisOperations() {


        /*
        // verify that we have nothing
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/cvdata")
                .then()
                .statusCode(200)
                .body("size()", is(0));
////////////fixme  *********
        // create a first increment key with an initial value of 0
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("hello","world");
        jsonObject.put("first-key:json", jsonObject1);
        CvData cvData = new CvData();
        cvData.key = "first-key:json";
        cvData.setValueString(jsonObject1.toString());

        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(cvData)
                .when()
                .post("/cvdata")
                .then()
                .statusCode(200)
                .body("key", is("first-key:json"))
                .body("value", is("{empty=false}"));

        /*
        // create a second increment key with an initial value of 10
        given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"key\":\"second-key\",\"value\":10}")
                .when()
                .post("/increments")
                .then()
                .statusCode(200)
                .body("key", is("second-key"))
                .body("value", is(10));

        // increment first key by 1
        given()
                .contentType(ContentType.JSON)
                .body("1")
                .when()
                .put("/increments/first-key")
                .then()
                .statusCode(204);

        // verify that key has been incremented
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/increments/first-key")
                .then()
                .statusCode(200)
                .body("key", is("first-key"))
                .body("value", is(1));

        // increment second key by 1000
        given()
                .contentType(ContentType.JSON)
                .body("1000")
                .when()
                .put("/increments/second-key")
                .then()
                .statusCode(204);

        // verify that key has been incremented
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/increments/second-key")
                .then()
                .statusCode(200)
                .body("key", is("second-key"))
                .body("value", is(1010));

        // verify that we have two keys in registered
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/increments")
                .then()
                .statusCode(200)
                .body("size()", is(2));

        // delete first key
        given()
                .accept(ContentType.JSON)
                .when()
                .delete("/increments/first-key")
                .then()
                .statusCode(204);

        // verify that we have one key left after deletion
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/increments")
                .then()
                .statusCode(200)
                .body("size()", is(1));

        // delete second key
        given()
                .accept(ContentType.JSON)
                .when()
                .delete("/increments/second-key")
                .then()
                .statusCode(204);

        // verify that there is no key left
        given()
                .accept(ContentType.JSON)
                .when()
                .get("/increments")
                .then()
                .statusCode(200)
                .body("size()", is(0));

         */
    }
}
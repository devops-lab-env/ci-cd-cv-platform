package org.cv.platform.models;


import lombok.Data;
import org.json.JSONObject;

@Data
public class CvData {
    public String key;
    public JSONObject value;

    public CvData(String key, JSONObject value) {
        this.key = key;
        this.value = value;
    }
    public CvData(){}
}

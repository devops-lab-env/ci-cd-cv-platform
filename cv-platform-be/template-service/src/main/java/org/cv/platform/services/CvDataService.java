package org.cv.platform.services;


import io.quarkus.redis.datasource.ReactiveRedisDataSource;
import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.keys.ReactiveKeyCommands;
import io.quarkus.redis.datasource.string.StringCommands;
import io.smallrye.mutiny.Uni;
import org.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class CvDataService {

    /**
     * Active Keys (Only One): DN:APP:Template:lang:active
     * keys (Duplicates): DN:App:Template:lang
     */

    // This quickstart demonstrates both the imperative
    // and reactive Redis data sources
    // Regular applications will pick one of them.

    private ReactiveKeyCommands<String> keyCommands;
    private StringCommands<String, String> jsonCommands;

    public CvDataService(RedisDataSource ds, ReactiveRedisDataSource reactive) {
        jsonCommands = ds.string(String.class);
        keyCommands = reactive.key();
    }


    public JSONObject get(String key) {
        JSONObject value = new JSONObject(jsonCommands.get(key));
        if (value == null) {
            return null;
        }
        return value;
    }

    public void set(String key, JSONObject value) {
        jsonCommands.set(key, value.toString());
    }

    public Uni<Void> del(String key) {
        return keyCommands.del(key)
                .replaceWithVoid();
    }

    public Uni<List<String>> keys() {
        return keyCommands.keys("*");
    }

    public JSONObject searchActiveDomainName(String domainName,String lang){

        Uni<List<String>> uniKeys = keyCommands.keys(domainName+"*:*:*:"+lang+":active");
        List<String> keys = uniKeys.await().indefinitely();
        if (keys.size() == 1)
            return new JSONObject(jsonCommands.get(keys.get(0)));
        else
            return new JSONObject();
    }

    public boolean checkActiveDomainName(String domainName, String lang){
        Uni<List<String>> uniKeys = keyCommands.keys(domainName+"*:*:*:"+lang+":active");
        List<String> keys = uniKeys.await().indefinitely();
        if (keys.size() == 1)
            return true;
        else
            return false;

    }
}
package org.cv.platform;

import org.cv.platform.services.CvDataService;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@Path("/")
public class MainResource {

    final String PATH_BASE_ASSETS = "/home/ubuntu/app/assets/";
    final String PATH_BASE_APP_VUE = "/home/ubuntu/app/dist/";

    @Inject
    CvDataService cvDataService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response home(@HeaderParam("host") String host) throws IOException {

        // [i18n]
        //List<Locale.LanguageRange> languages =Locale.LanguageRange.parse(context.request().getHeader("Accept-Language"));
        //Locale.LanguageRange.parse(context.request().getHeader("Accept-Language"));

        Response response;
        host = host.split(":")[0];

        if (!cvDataService.checkActiveDomainName(host, "en"))
            response = Response.status(Response.Status.BAD_REQUEST).build();
        else {
            try {
                File app = new File(PATH_BASE_APP_VUE + "index.html");
                response = Response.ok(Files.readString(app.toPath(), StandardCharsets.UTF_8)).build();
            } catch (IOException ioE) {
                response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
        }
        return response;
    }

    @GET
    @Path("/{fileType}/{fileName}")
    @Produces("*/*")
    public Response hello(@HeaderParam("host") String host, @PathParam("fileType") String fileType, @PathParam("fileName") String fileName) {
        Response response;
        host = host.split(":")[0];
        JSONObject jsonObject = cvDataService.searchActiveDomainName(host, "en");
        JSONObject cv = jsonObject.getJSONObject("cv");
        if (!jsonObject.isEmpty()) {
            try {
                String pathAssets = PATH_BASE_ASSETS + cv.getString("domainName") + "/" + cv.getString("app") + "/" + cv.getString("template") + "/" + fileType + "/" + fileName;

                File asset = new File(pathAssets);
                if (asset.exists())
                    response = Response.ok(Files.readAllBytes(asset.toPath())).build();
                else
                    response = Response.status(Response.Status.NOT_FOUND).build();
            } catch (IOException ioE) {
                response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } catch (Exception ex) {
                response = Response.status(Response.Status.BAD_REQUEST).build();
            }
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }
}

package org.cv.platform.resources;

import com.fasterxml.jackson.databind.util.JSONPObject;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.smallrye.mutiny.Uni;
import org.cv.platform.models.CvData;
import org.cv.platform.models.Increment;
import org.cv.platform.services.CvDataService;
import org.cv.platform.services.IncrementService;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RegisterForReflection
@Path("/api/cv")
public class CvDataResource {

    @Inject
    CvDataService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getCv(@HeaderParam("host") String host){
        host = host.split(":")[0];
        JSONObject jsonObject = service.searchActiveDomainName(host,"en");
        return jsonObject.toString();
    }

    @GET
    @Path("keys")
    public Uni<List<String>> keys() {
        return service.keys();
    }

    @POST
    public CvData create(CvData cvData) {
        service.set(cvData.key, cvData.getValue());
        return cvData;
    }

    @GET
    @Path("/{key}")
    public CvData get(String key) {
        return new CvData(key, service.get(key));
    }

    /*
    @PUT
    @Path("/{key}")
    public void increment(String key, long value) {
        service.increment(key, value);
    }
    */

    @DELETE
    @Path("/{key}")
    public Uni<Void> delete(String key) {
        return service.del(key);
    }
}